(function(window, $) {
  //Sticky Header Init
  var loadPage = (function() {
      return {
          init: function() {
              $(window).load(function() {
                  $(".loader").fadeOut("slow");
                  $("#overlayer").fadeOut("slow");
              })
          }
      }
  }());
  //Sticky Header Init
  var headerSticky = (function() {
      return {
          init: function() {
              $(window).scroll(function() {
                  if ($(window).scrollTop() >= 44) {
                      $('.sticky-header').addClass('fixed-header');
                  } else {
                      $('.sticky-header').removeClass('fixed-header');
                  }
              });
          }
      }
  }());
  //Slick Slider Init
  var slickSlider = (function() {
      return {
          init: function() {
              if ($('.slick-multiple-items').length > 0) {
                  $('.slick-multiple-items').slick({
                      infinite: true,
                      slidesToShow: 5,
                      slidesToScroll: 1,
                      responsive: [{
                          breakpoint: 768,
                          settings: {
                              arrows: false,
                              slidesToShow: 2
                          }
                      }, {
                          breakpoint: 480,
                          settings: {
                              arrows: false,
                              slidesToShow: 1
                          }
                      }]
                  });
              }
          }
      }
  }());


  var datePicker = (function() {
        return {
            init: function() {
                $('.jsDatepicker').datepicker();    
            }
        }
    }());


    /*dev*/
    var stickySidebar = (function() {
            return {
                init: function() {
                    if ($('.js-stickyaside').length == 1) {
                        $('.js-stickyaside').stickySidebar({
                            topSpacing: 40,
                            bottomSpacing: 0,
                            containerSelector: '.stickyWrap',
                            minWidth: 992
                        });
                    }
                }
        }
    }());
  $(function() {
      new WOW().init();
      loadPage.init();
      headerSticky.init();
      slickSlider.init();
      datePicker.init();
      stickySidebar.init();
  });
}(window, jQuery));